extends RigidBody

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
onready var w = get_node("/root/water")
var time = 0


const BUOYANCY_MULTIPLIER = 1
const WATER_DENSITY = 1000
const GRAVITY = 9.8
const PROBE_X_OFFSET = 1
enum {UP, DOWN}
enum {MOTOR, SAIL}
export var drive = MOTOR
#var mass = get_mass()
export var density = 500
export var probe_count = 4


onready var front = get_node("front")
onready var back = get_node("back")

onready var baum = get_node("Scene Root/baum")

var wind_speed = 1
var wind_dir = Vector3(1.0, 0.0, 0.0) #wind_dir is a normalized Direction Vector to which the Wind is going _NOT_ Where it comes from
const SPEED_GRADIENT = -0.64 # m for the speed function m*wind_dir + 2
const DRAG_FACTOR = 0.5

var baum_rot_weight = 0.1



func _ready():
	var pos = get_global_transform().origin
	pos.y = w.wave_formula(pos.x, pos.z, time)
	set_translation(pos)
	set_physics_process(true)


func _physics_process(delta):
	time += delta

	var front_point = front.get_global_transform().origin
	var back_point = back.get_global_transform().origin
	var boat_normal = (front_point - back_point).normalized()


	var wind_point = (wind_dir + get_global_transform().origin).normalized()
	var wind_side = sign(boat_normal.cross(wind_dir).y)
	#If wind comes from port (Backbord) wind_side is positive.
	#If it comes from starport (Steuerport) it is negative.
	#If the wind comes directly from behind wind_side will be 0
	print(wind_side)

	print(baum.get_rotation())
	#--TODO-- Handle wind from behind!
	if Input.is_key_pressed(KEY_A):
		#if wind_side == 1:
		baum.rotate_y(-0.005*((baum.get_rotation().y - (1/4 * PI))*1.5))
		#else:

		#baum.rotate_y(-0.005*(abs(baum.get_rotation().y) + 1))
	if Input.is_key_pressed(KEY_D):
		if wind_side == 1:
			baum.rotate_y(0.005*(baum.get_rotation().y + 1))
		else:
			baum.rotate_y(-0.005*(baum.get_rotation().y*1.25))




func _integrate_forces(state):
	var global_pos = get_global_transform().origin
	var top_normal = (global_pos - get_node("top").get_global_transform().origin).normalized()

	var front_pos = get_node("front").get_global_transform().origin
	var front_local = get_node("front").get_translation()
	var back_pos = get_node("back").get_global_transform().origin
	var back_local = get_node("back").get_translation()

	var bb_pos = get_node("bb").get_global_transform().origin
	var bb_local = get_node("bb").get_translation()
	var sb_pos = get_node("sb").get_global_transform().origin
	var sb_local = get_node("sb").get_translation()


	if global_pos.y < w.wave_formula(global_pos.x, global_pos.z, time):
		var buoyancy = global_pos
		buoyancy.y += buoyancy_force(global_pos)
		state.add_force(Vector3(0, buoyancy.y, 0), Vector3(0, 0, 0))

	add_force_to_probe(front_pos, front_local, global_pos, top_normal, state)
	add_force_to_probe(back_pos, back_local, global_pos, top_normal, state)

	add_force_to_probe(bb_pos, bb_local, global_pos, top_normal, state)
	add_force_to_probe(sb_pos, sb_local, global_pos, top_normal, state)

	if Input.is_key_pressed(KEY_W):
		var drive_force = 10000
		var boat_up = (global_pos - get_node("top").get_global_transform().origin).normalized()
		var drive_pos = back_local.rotated(boat_up, get_rotation().y)
		var boat_normal = (back_pos - front_pos).normalized()
		drive_force = boat_normal * drive_force
		state.add_force(drive_force, boat_normal* 2) #This actually works


func add_force_to_probe(probe_position, probe_local_pos, global_pos, top_normal, state):
	var probe_normal = (probe_position - global_pos).normalized() * PROBE_X_OFFSET
	if probe_position.y < w.wave_formula(probe_position.x, probe_position.z, time):
		var buoyancy = probe_local_pos
		buoyancy.y += probe_buoyancy_force(probe_position, UP)
		state.add_force(Vector3(0, buoyancy.y, 0), probe_normal)
	else:
		var buoyancy = probe_local_pos
		buoyancy.y -= probe_buoyancy_force(probe_position, DOWN)
		state.add_force(Vector3(0, buoyancy.y, 0), probe_normal)

func buoyancy_force(position):
	#var yforce = rho * 9.8 * (w.wave_formula(position.x, position.z, time) - position.y) * BUOYANCY_MULTIPLIER
	var volume = mass / density
	var yforce = WATER_DENSITY * GRAVITY * volume * distance_to_water(position) * 0.5
	#print("B FORCE: " + str(yforce))
	return yforce


func probe_buoyancy_force(position, direction):
	#var yforce = rho * 9.8 * (w.wave_formula(position.x, position.z, time) - position.y) * BUOYANCY_MULTIPLIER
	var volume = mass / density
	var yforce
	if direction == UP:
		yforce = WATER_DENSITY * volume * GRAVITY * distance_to_water(position) * 0.05
	else:
		yforce = volume * GRAVITY * 0.25
	return yforce / probe_count



func distance_to_water(position):
	var wave_height = w.wave_formula(position.x, position.z, time)
	var dist = wave_height - position.y
	#print("DIST TO SURF: " + str(dist))
	return dist * 2



