
extends ImmediateGeometry

const SAIL_MATERIAL = preload("res://Meshes/sailmat.material")
onready var fock_o = get_node("fock_o").get_translation()
onready var fock_u = get_node("fock_u").get_translation()
var wind_dir = Vector3(0.125, 0, 0)
var wind_strenght = 0

func _ready():
	var fock_schot = get_node("bb_schot").get_translation()
	var fock_umid = fock_schot.linear_interpolate(fock_u, 0.5)

	set_material_override(SAIL_MATERIAL)
	self.begin(Mesh.PRIMITIVE_TRIANGLES)
	#Triangle 1
	set_vertex_normal(fock_o, fock_umid, fock_u)
	self.add_vertex(fock_o)
	set_vertex_normal(fock_o, fock_umid, fock_u)
	self.add_vertex(fock_umid + wind_dir * wind_strenght)
	set_vertex_normal(fock_o, fock_umid, fock_u)
	self.add_vertex(fock_u)
	#Triangle 2
	set_vertex_normal(fock_o, fock_schot, fock_umid)
	self.add_vertex(fock_o)
	set_vertex_normal(fock_o, fock_schot, fock_umid)
	self.add_vertex(fock_schot)
	set_vertex_normal(fock_o, fock_schot, fock_umid)
	self.add_vertex(fock_umid + wind_dir * wind_strenght)

	self.end()

func set_vertex_normal(top, bottom, front): #Calculate the faces normal
	var edge_bottom = front - bottom
	var edge_rear = top - bottom
	var normal = edge_bottom * edge_rear
	set_normal(normal.inverse())
	
	

