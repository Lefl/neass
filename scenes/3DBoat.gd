extends RigidBody

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
var time = 0
var probes = {"front_left" : Vector3(-1, 0, 2), "front_right" : Vector3(1, 0, 2), "back_left" : Vector3(-1, 0, -2), "back_right" : Vector3(1, 0, -2)}

func _ready():
	set_physics_process(true)

func _physics_process(delta):
	time += delta
	var height = wave_formula(null, get_translation().z, time)
	#set_translation(Vector3(get_translation().x, height, get_translation().z))

func _integrate_forces(state):
	print(get_angular_velocity())
	var force_factor = 0.5
	state.add_force(Vector3(0, get_probe_force(probes.front_left) * force_factor, 0), probes.front_left)
	
	state.add_force(Vector3(0, get_probe_force(probes.front_right) * force_factor, 0) , probes.front_right)
	
	#state.add_force(Vector3(0, get_probe_force(probes.back_left), 0) * force_factor, probes.back_left)
	
	#state.add_force(Vector3(0, get_probe_force(probes.back_right), 0) * force_factor, probes.back_right)



func get_probe_force(probe):
	var probe_global = probe_get_global_pos(probe)

	if probe_global.y <= wave_formula(null, probe_global.z, time):
		var new_height = wave_formula(null, probe.z, time)
		var probe_force = new_height - probe.y
		#print("Force for Probe: " + str(probe) + ": " + str(probe_force))
		return probe_force
	else:
		return 0

func probe_get_global_pos(probe):
	var probe_global = get_global_transform().origin + probe
	return probe_global

func wave_formula(x, z , t):
	var y = sin(0.25 * z - t)
	return y
	


func _on_RigidBody_body_entered( body ):
	print("gg")
