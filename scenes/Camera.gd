extends Camera

# class member variables go here, for example:
# var a = 2
var mouse_sensitivity = 1
var yaw = 0
var pitch = 0

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	pass

func _process(delta):
	var dir = Vector3()
	var cam_xform = get_global_transform()
	if Input.is_key_pressed(KEY_A):
		dir += -cam_xform.basis[0]
	if Input.is_key_pressed(KEY_D):
		dir += cam_xform.basis[0]


	if Input.is_key_pressed(KEY_W):
		dir += -cam_xform.basis[2]
	if Input.is_key_pressed(KEY_S):
		dir += cam_xform.basis[2]
		
	dir.y = 0
	dir = dir.normalized()

	var target = dir
	get_tree().get_root().get_node("ocean/yaw").translate(target)

func _input(event):

	if event is InputEventMouseMotion:
		pitch = max(min(pitch - event.relative.y * mouse_sensitivity, 85), -85)
		yaw = fmod(yaw - event.relative.x * mouse_sensitivity, 360)
		get_tree().get_root().get_node("ocean/yaw").set_rotation(Vector3(0, deg2rad(yaw), 0))
		get_tree().get_root().get_node("ocean/yaw/pitch").set_rotation(Vector3(deg2rad(pitch), 0, 0))
