extends Node2D
#
const NORTH = Vector2(0.0, 1.0)
const EAST = Vector2(1.0, 0.0)
const SOUTH = Vector2(0.0, 1.0)
const WEST = Vector2(-1.0, 0.0)
const SPEED_GRADIENT = -0.64 # m for the speed function m*wind_dir + 2
const DRAG_FACTOR = 0.5

var wind_dir = Vector2(1.0, 0.0)



onready var arrow = get_node("directionarrow")
onready var boat = get_node("boat")
onready var labels = get_node("dbglabels")


func _ready():
	set_physics_process(true)
func _physics_process(delta):
	var wind_speed = get_node("wspeed").get_value()
	var wind_angle = wind_dir.angle_to(NORTH)
	arrow.set_rotation(wind_angle)


	var boat_normal = Vector2(0, -1).rotated(boat.get_rotation())
	var wind_rel_dir = boat_normal.angle_to(wind_dir)

	
	var direction = wind_dir + boat_normal

	var boat_speed = (-0.64*abs(wind_rel_dir) + 2) * wind_speed
	
	var wind_drag = wind_dir * wind_speed * DRAG_FACTOR
	var boat_velocity = wind_drag + boat_normal * wind_speed 
	
	
	
	
	boat.get_node("Line2D").look_at(boat.get_global_position() + wind_drag)
	boat.get_node("velocity").look_at(boat.get_global_position() + boat_velocity)
	boat.get_node("boatdir").look_at(boat.get_global_position() + boat_normal)

	labels.get_node("wdir").set_text("Wind Direction: " + str(wind_angle))
	labels.get_node("wreldir").set_text("Relative Wind Direction: " + str(wind_rel_dir))
	labels.get_node("wspeed").set_text("Wind Speed: " + str(wind_speed))
	labels.get_node("boatnormal").set_text("Boat Normal: " + str(boat_normal))
	labels.get_node("wdrag").set_text("Wind Drag: " + str(wind_drag)) 
	boat.get_node("speed").set_text("Boat Speed: " + str(boat_speed))


func _input(event):
	if event is InputEventKey:
		if event.scancode == KEY_LEFT and event.is_pressed():
			boat.set_rotation(boat.get_rotation() - 0.05)
		if event.scancode == KEY_RIGHT and event.is_pressed():
			boat.set_rotation(boat.get_rotation() + 0.05)
		if event.scancode == KEY_SPACE and event.is_pressed():
			boat.set_visible(!boat.is_visible())

